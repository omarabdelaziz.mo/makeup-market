from django.contrib import messages
from django.shortcuts import redirect


def map_data(strategy, details, backend, user=None, *args, **kwargs):
    print(backend.name)
    if backend.name == "facebook":
        name = details.get("fullname")
        details["name"] = name
    elif backend.name == "google-oauth2":
        name = details.get("fullname")
        details["name"] = name


def mail_validation(strategy, details, user=None, is_new=False, *args, **kwargs):
    if user and user.email:
        return  # The user we're logging in already has their email attribute set
    elif is_new and not details.get("email"):
        # If we're creating a new user, and we can't find the email in the details
        # we'll attempt to request it from the data returned from our backend strategy
        userEmail = strategy.request_data().get("email")
        if userEmail:
            details["email"] = userEmail
        else:
            # If there's no email information to be had, we need to ask the user to fill it in
            # This should redirect us to a view
            messages.error(strategy.request, "InValid email address")
            return redirect("account:login")


def activate_social_user(strategy, details, user=None, is_new=False, *args, **kwargs):
    if user:
        user.is_active = True
        user.save()
