from decimal import Decimal

from django.conf import settings
from makeup_market.apps.catalogue.models import Product
from makeup_market.apps.checkout.models import DeliveryOptions


class Basket:
    """
    A base class , providing some defualt behaviors that
    can be inherited or overrided, as necessary
    """

    def __init__(self, request):

        self.session = request.session
        basket = self.session.get(settings.BASKET_SESSION_ID)
        if settings.BASKET_SESSION_ID not in request.session:
            basket = self.session[settings.BASKET_SESSION_ID] = {}
        self.basket = basket

    def add(self, product, qty):
        """
        Add or update the users basket session data
        """
        product_id = str(product.id)

        if product_id not in self.basket:
            self.basket[product_id] = {"price": float(product.regular_price), "qty": int(qty)}
        else:
            self.basket[product_id]["qty"] = int(qty)

        self.save()

    def update(self, product, qty):
        """
        Update values in  sesssion data
        """
        product_id = str(product)
        qty = qty

        if product_id in self.basket:
            self.basket[product_id]["qty"] = qty

        self.save()

    def delete(self, product):
        """
        Delete item from session data
        """
        product_id = str(product)

        if product_id in self.basket:
            del self.basket[product_id]
            self.save()

    def __iter__(self):
        """
        Collect the product id in the session data to query database
        and return products
        """
        product_ids = self.basket.keys()
        products = Product.objects.filter(id__in=product_ids)
        basket = self.basket.copy()

        for product in products:
            basket[str(product.id)]["product"] = product

        for item in basket.values():
            item["total_price"] = item["price"] * int(item["qty"])
            yield item

    def __len__(self):
        """
        Get the basket data and count the qty of items
        """
        return sum(int(item["qty"]) for item in self.basket.values())

    def get_subtotal_price(self):
        return round(sum(float(item["price"]) * int(item["qty"]) for item in self.basket.values()), 2)

    def get_total_price(self):
        delivery_price = 0.00
        subtotal = sum(float(item["price"]) * int(item["qty"]) for item in self.basket.values())
        if "purchase" in self.session:
            delivery_price = DeliveryOptions.objects.get(id=self.session["purchase"]["delivery_id"]).delivery_price
        total = subtotal + float(delivery_price)
        return round(total, 2)

    def get_delivery_price(self):
        delivery_price = 0.00
        if "purchase" in self.session:
            delivery_price = DeliveryOptions.objects.get(id=self.session["purchase"]["delivery_id"]).delivery_price
        return delivery_price

    def basket_update_delivery(self, delivery_price=0):
        subtotal = sum(float(item["price"]) * int(item["qty"]) for item in self.basket.values())
        total = subtotal + float(delivery_price)
        return round(total, 2)

    def clear(self):
        """
        Remove basket from session
        """

        del self.session[settings.BASKET_SESSION_ID]
        del self.session["address"]
        del self.session["purchase"]
        self.save()

    def save(self):
        self.session.modified = True
