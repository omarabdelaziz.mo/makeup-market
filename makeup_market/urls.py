"""makeup_market URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("makeup_market.apps.catalogue.urls", namespace="catalogue")),
    path("basket/", include("makeup_market.apps.basket.urls", namespace="basket")),
    path("checkout/", include("makeup_market.apps.checkout.urls", namespace="checkout")),
    path("account/", include("makeup_market.apps.account.urls", namespace="account")),
    path("social-auth/", include("social_django.urls", namespace="social")),
    path("orders/", include("makeup_market.apps.orders.urls", namespace="orders")),
    path("__debug__/", include(debug_toolbar.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
